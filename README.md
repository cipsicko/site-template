**This is a simple start for yours web projects**

I've insert the essential (imho) for a light web stuff. Below you'll find how to simple run all.

## How to install
1. type ```npm start``` in your console
2. take a coffe
3. DONE!

## What i have now?
In your project folder you'll see something like this:

```
+-- my-project
    |___bower_components
    |
    |___dev
    |   |___includes
    |   |   |   footer.php
    |   |   |   header.php
    |   |   |   settings.php
    |   |   
    |   |___pages
    |   |   |   home.php
    |   |   
    |   |___scripts
    |   |   |   main.js
    |   |
    |   |___styles
    |   |   |   normalize.css
    |   |   |
    |   |   |___scss
    |   |   |   |   main.scss
    |   |   |   |___partials
    |   |   
    |   |___views
    |   |   |   footer.jade
    |   |   |   home.jade
    |   |   
    |   |   .htaccess
    |   |   index.php
    |    
    |___node_modules
    |   
    |   .gitignore
    |   bower.json
    |   gulpconfig.json
    |   gulpfile.js
    |   package.json
    |   README.md
```

**Enjoy**