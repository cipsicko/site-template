'use-strict'
//include gulp
const 	gulp = require('gulp'),
		config = require('./gulpconfig.json'),
        del = require('del'),
        merge = require('merge-stream');

// production mode (see build task)
var isProduction = false;

//include plugin
var plugins = require("gulp-load-plugins")({
    pattern: ['gulp-*', 'gulp.*', 'main-bower-files'],
    replaceString: /\bgulp[\-.]/
});


gulp.task('styles', function() {

    plugins.util.log(plugins.util.colors.bgYellow('Starting building styles...'));

    var sassStream,
        cssStream,
        cssnanoOpts = {
            safe: true,
            discardUnused: false, // no remove @font-face
            reduceIdents: false, // no change on @keyframes names
            zindex: false // no change z-index
        };

    sassStream = gulp.src(config.input.sass)
        .pipe(plugins.sass({
            errLogToConsole: true
        }))
        .pipe(plugins.if(isProduction, plugins.cssnano(cssnanoOpts)));

    cssStream = gulp.src(config.input.styles)
        .pipe(plugins.if(isProduction, plugins.cssnano(cssnanoOpts)));

    return merge(cssStream, sassStream)
        .pipe(plugins.sourcemaps.init())
        .pipe(plugins.concat('main.css'))
        .pipe(plugins.sourcemaps.write('./maps'))
        .pipe(gulp.dest(config.output.styles));

});

gulp.task('script', function() {

    plugins.util.log(plugins.util.colors.bgYellow('Starting building scripts...'));

    gulp.src(plugins.mainBowerFiles('**/*.js').concat(config.input.scripts))
        .pipe(plugins.concat('main.js'))
		.pipe(plugins.if(isProduction, plugins.rename({
			suffix: ".min"
		})))
        .pipe( plugins.if(isProduction, plugins.uglify()))
        .pipe(gulp.dest(config.output.scripts));
});

gulp.task('views', function() {

    plugins.util.log(plugins.util.colors.bgYellow('Starting building views...'));

	gulp.src(config.input.views)
		.pipe(plugins.jade())
		.pipe(gulp.dest(config.output.views));
});

gulp.task('pages', function() {
    gulp.src(config.input.pages)
        .pipe(gulp.dest(config.output.pages));

    gulp.src(config.input.base)
        .pipe(gulp.dest(config.output.base));
});

gulp.task('includes', function() {
    gulp.src(config.input.includes)
        .pipe(gulp.dest(config.output.includes));
});

gulp.task('dev', ['styles', 'script', 'views', 'includes', 'pages'], function () {

    //copy base files to app folder
    gulp.src(config.input.base)
        .pipe(gulp.dest(config.output.base));

    gulp.watch(config.input.styles, ['styles']);
    gulp.watch(config.input.sass, ['styles']);
    gulp.watch(config.input.scripts, ['script']);
    gulp.watch(config.input.views, ['views']);
    gulp.watch(config.input.includes, ['includes']);
    gulp.watch(config.input.pages, ['pages']);

    plugins.util.log(plugins.util.colors.bgYellow(' Starting watching files, you can edit now '));
});

gulp.task('build', ['prod', 'styles', 'script', 'views', 'includes', 'pages'], function () {
    plugins.util.log(plugins.util.colors.bgYellow('Starting production build...'));
});

gulp.task('prod', function(){
    isProduction = true;
    plugins.util.log(plugins.util.colors.bgYellow('Starting production build...'));
});

// Remove all files from the build paths
gulp.task('clean', function(done) {
    del('./app/**', {
        force: true
    }).then(function() { done(); });
});